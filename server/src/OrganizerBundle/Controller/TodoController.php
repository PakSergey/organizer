<?php

namespace OrganizerBundle\Controller;

use OrganizerBundle\Entity\Todo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class TodoController extends Controller
{
    public function getListAction() {
        $em = $this->getDoctrine()->getManager();
        $todoObjects = $em->getRepository(Todo::class)
            ->findAll();
        $todo = [];
        foreach ($todoObjects as $todoObject) {
            $todo[] = [
                "id"      => $todoObject->getId(),
                "status"  => $todoObject->getStatus(),
                "value"   => $todoObject->getValue()
            ];
        }
        $response   = new Response();
        $response->setContent(json_encode($todo));
        return $response;
    }

    public function deleteByIdAction($todoId) {
        $em = $this->getDoctrine()->getManager();
        $todo = $em->getRepository(Todo::class)->find($todoId);
        if (!$todo) {
            throw $this->createNotFoundException(
                'No todo found for id '.$todoId
            );
        }
        $em->remove($todo);
        $em->flush();
        return new Response();
    }

    public function addTodoAction() {
        $em   = $this->getDoctrine()->getManager();
        $todo = new Todo();
        $todo->setStatus(isset($_POST["status"])? $_POST["status"] : "active");
        $todo->setValue(isset($_POST["text"])? $_POST["text"] : "");
        $em->persist($todo);
        $em->flush();
        return new Response();
    }

}