<?php

namespace OrganizerBundle\Controller;


use OrganizerBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PageController extends Controller
{
    public function getPageAction()
    {
        $bookId = !empty($_POST["bookId"]) ? $_POST["bookId"] : null;
        $numberOfPage = !empty($_POST["numberOfPage"]) ? $_POST["numberOfPage"] : null;
        $options = compact("bookId", "numberOfPage");
        $pages = $this->getDoctrine()->getManager()->getRepository(Page::class)->findBy(
            $options
        );
        $page = new Page();
        if (!empty($pages)) {
            $page = $pages[0];
        } else {
            $page->setBookId($bookId);
            $page->setNumberOfPage($numberOfPage);
            $page->setContent("");
        }
        $response = new Response();
        $response->setContent(
            json_encode(
                [
                    "bookId"       => $page->getBookId(),
                    "numberOfPage" => $page->getNumberOfPage(),
                    "content"      => $page->getContent()
                ]
            )
        );
        return $response;
    }

    public function savePageAction()
    {
        if(!empty($_POST["content"])) {
            $content = $_POST["content"];
        } else {
            return new Response();
        }
        $bookId = !empty($_POST["bookId"]) ? $_POST["bookId"] : null;
        $numberOfPage = !empty($_POST["numberOfPage"]) ? $_POST["numberOfPage"] : null;
        $options = compact("bookId", "numberOfPage");
        $pages = $this->getDoctrine()->getManager()->getRepository(Page::class)->findBy(
            $options
        );
        if(empty($pages)) {
            $page = new Page();
        } else {
            $page = $pages[0];
        }
        $page->setContent($content);
        $page->setBookId($bookId);
        $page->setNumberOfPage($numberOfPage);
        $em = $this->getDoctrine()->getManager();
        $em->persist($page);
        $em->flush();
        return new Response();
    }
}