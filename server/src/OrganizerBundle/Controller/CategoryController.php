<?php

namespace OrganizerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    public function getAllAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categoryObjects = $em->getRepository('OrganizerBundle:Category')
            ->findAll();
        $categories = [];
        foreach ($categoryObjects as $categoryObject) {
            $categories[] = [
                "id"    => $categoryObject->getId(),
                "name"  => $categoryObject->getName()
            ];
        }
        $response   = new Response();
        $response->setContent(json_encode($categories));
        return $response;
    }

    public function getCategoriesByNameAction()
    {
        $name = $_POST["categoryName"];
        $em = $this->getDoctrine()->getManager();
        $categoryObjects = $em->getRepository("OrganizerBundle:Category")->createQueryBuilder("cat")
            ->where("cat.name LIKE :name")
            ->setParameter("name", "$name%")
            ->getQuery()
            ->getResult();
        $categories = [];
        foreach ($categoryObjects as $categoryObject) {
            $categories[] = [
                "id" => $categoryObject->getId(),
                "name" => $categoryObject->getName()
            ];
        }
        $response = new Response();
        $response->setContent(json_encode($categories));
        return $response;
    }

    public function getCategoryByIdAction ($categoryId)
    {
        $em       = $this->getDoctrine()->getManager();
        $category = $em->getRepository('OrganizerBundle:Category')
            ->find($categoryId);
        $response = new Response();
        $response->setContent(json_encode(
            [
                "id"    => $category->getId(),
                "name"  => $category->getName()
            ]
        ));
        return $response;
    }
}