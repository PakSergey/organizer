<?php

namespace OrganizerBundle\Controller;

use Doctrine\ORM\Tools\Pagination\Paginator;
use OrganizerBundle\Entity\Word;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class WordController extends Controller
{
    const WORDS_FOR_ALL_TIME = "All";
    const WORDS_FOR_TODAY    = "Today";
    const DEFAULT_WORDS_COUNT = 20;

    public function getWordsAction()
    {
        $limit = 30;
        $page  = $_POST["page"]? $_POST["page"] : 1;
        $em    = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Word::class)->createQueryBuilder('x')
            ->select()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit)
            ->getQuery();

        $paginator = new Paginator($query, $fetchJoinCollection = false);
        $response = new Response();
        $words = [];
        foreach ($paginator as $word) {
            $words[] = [
                "id"      => $word->getId(),
                "english" => $word->getEnglish(),
                "russian" => $word->getRussian()
            ];
        }
        $response->setContent(json_encode($words));
        return $response;
    }

    public function getWordsByNameAction()
    {
        $name = !empty($_POST["name"])? $_POST["name"] : "";
        $limit = 30;
        $page  = $_POST["page"]? $_POST["page"] : 1;
        $em    = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Word::class)->createQueryBuilder('x')
            ->andWhere("x.russian LIKE :name OR x.english LIKE :name")
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit)
            ->setParameter("name", "$name%")
            ->getQuery();

        $paginator = new Paginator($query, $fetchJoinCollection = false);
        $response = new Response();
        $words = [];
        foreach ($paginator as $word) {
            $words[] = [
                "id"      => $word->getId(),
                "english" => $word->getEnglish(),
                "russian" => $word->getRussian()
            ];
        }
        $response->setContent(json_encode($words));
        return $response;
    }

    public function getTotalCountAction()
    {
        $repo = $this   ->getDoctrine()
            ->getManager()
            ->getRepository(Word::class);

        $qb = $repo->createQueryBuilder('x');
        $qb->select('COUNT(x)');
        $totalCount = $qb->getQuery()->getSingleScalarResult();
        $response = new Response();
        $response->setContent(json_encode([compact("totalCount")]));
        return $response;
    }

    public function getTotalCountByNameAction()
    {
        $name = !empty($_POST["name"]) ? $_POST["name"] : "";
        $repo = $this   ->getDoctrine()
            ->getManager()
            ->getRepository(Word::class);

        $qb = $repo->createQueryBuilder('x')
            ->select('COUNT(x)')
            ->where("x.russian LIKE :name OR x.english LIKE :name")
            ->setParameter("name", "$name%");
        $totalCount = $qb->getQuery()->getSingleScalarResult();
        $response = new Response();
        $response->setContent(json_encode([compact("totalCount")]));
        return $response;
    }

    public function addWordAction()
    {
        $english = $_POST["english"] ? $_POST["english"] : "";
        $russian = $_POST["russian"] ? $_POST["russian"] : "";
        $createdTime = date("Y-m-d");
        $em = $this->getDoctrine()->getManager();
        $word = new Word();
        $word->setEnglish($english);
        $word->setRussian($russian);
        $word->setCreatedTime($createdTime);
        $em->persist($word);
        $em->flush();
        return new Response();
    }

    public function getWordsByOptionsAction()
    {
        $typeOfWords = !empty($_POST["typeOfWords"]) ? $_POST["typeOfWords"] : self::WORDS_FOR_ALL_TIME;
        $count = !empty($_POST["count"]) ? $_POST["count"] : self::DEFAULT_WORDS_COUNT;
        $repo = $this   ->getDoctrine()
            ->getManager()
            ->getRepository(Word::class);
        $dateParam = "";
        if ($typeOfWords == self::WORDS_FOR_TODAY) {
            $dateParam = date("Y-m-d");
        }
        $words = $repo->createQueryBuilder('x')
            ->select()
            ->where("x.created LIKE :name")
            ->setParameter("name", "$dateParam%")
            ->getQuery()
            ->getResult();
        if ($typeOfWords == self::WORDS_FOR_ALL_TIME) {
            $filteredWords = [];
            if(count($words) > $count) {
                while (count($filteredWords) != $count) {
                    $index = rand(0, count($words) - 1);
                    if (!in_array($words[$index], $filteredWords)) {
                        $filteredWords[] = $words[$index];
                    }
                }
                $words = $filteredWords;
            }
        }
        $wordsArray = [];
        $response = new Response();
        foreach ($words as $word) {
            $wordsArray[] = [
                "id"      => $word->getId(),
                "english" => $word->getEnglish(),
                "russian" => $word->getRussian()
            ];
        }
        $response->setContent(json_encode($wordsArray));
        return $response;
    }
}