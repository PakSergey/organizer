<?php

namespace OrganizerBundle\Controller;

use OrganizerBundle\Entity\Category;
use OrganizerBundle\Entity\Knowledge;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class KnowledgeController extends Controller
{
    public function getKnowledgesByCategoryAction($categoryId)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('OrganizerBundle:Knowledge');
        $knowledgeObjects = $repository->findBy(
            [
                "categoryId" => $categoryId
            ]
        );
        $knowledges = [];
        foreach ($knowledgeObjects as $knowledgeObject) {
            $knowledges[] = [
                "id"          => $knowledgeObject->getId(),
                "categoryId"  => $knowledgeObject->getCategoryId(),
                "content"     => $knowledgeObject->getContent(),
                "title"       => $knowledgeObject->getTitle(),
            ];
        }
        $response   = new Response();
        $response->setContent(json_encode($knowledges));
        return $response;
    }

    public function getKnowledgeByIdAction($knowledgeId)
    {
        $em = $this->getDoctrine()->getManager();
        $knowledge = $em->getRepository('OrganizerBundle:Knowledge')
            ->find($knowledgeId);
        $response = new Response();
        $response->setContent(json_encode(
            [
                "id"          => $knowledge->getId(),
                "title"       => $knowledge->getTitle(),
                "content"     => $knowledge->getContent(),
                "categoryId"  => $knowledge->getCategoryId()
            ]
        ));
        return $response;
    }

    public function addKnowledgeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $knowledge = new Knowledge();
        $categoryRep = $em->getRepository('OrganizerBundle:Category');
        $categoryId = $categoryRep->getCategoryIdByName($_POST["categoryName"]);
        if (empty($categoryId)) {
            $category = new Category();
            $category->setName($_POST["categoryName"]);
            $em->persist($category);
            $em->flush();
            $categoryId = $category->getId();
        }
        $knowledge->setCategoryId($categoryId);
        $knowledge->setContent(isset($_POST["content"])? $_POST["content"] : "none");
        $knowledge->setTitle(isset($_POST["title"])? $_POST["title"] : "none");
        $knowledge->setCreatedTime(date("Y-m-d"));
        $em->persist($knowledge);
        $em->flush();
        return new Response();
    }

    public function updateKnowledgeAction() {
        $knowledgeId      = $_POST["id"];
        $knowledgeContent = $_POST["content"];
        $em = $this->getDoctrine()->getManager();
        $knowledge = $em->getRepository(Knowledge::class)->find($knowledgeId);
        if (!$knowledge) {
            throw $this->createNotFoundException(
                'No knowledge found for id '.$knowledgeId
            );
        }
        $knowledge->setContent($knowledgeContent);
        $knowledge->setCreatedTime(date("Y-m-d"));
        $em->flush();
        return new Response();
    }

    public function deleteKnowledgeAction($knowledgeId) {
        $em = $this->getDoctrine()->getManager();
        $knowledge = $em->getRepository(Knowledge::class)->find($knowledgeId);
        $categoryId = $knowledge->getCategoryId();
        if (!$knowledge) {
            throw $this->createNotFoundException(
                'No knowledge found for id '.$knowledgeId
            );
        }
        $em->remove($knowledge);
        $em->flush();
        $knowledgesByThisCategory = $em->getRepository(Knowledge::class)->findBy(
            [
                "categoryId" => $categoryId
            ]
        );
        if (empty($knowledgesByThisCategory)) {
            $category = $em->getRepository(Category::class)->find($categoryId);
            $em->remove($category);
            $em->flush();
        }
        return new Response();
    }

    public function getKnowledgesByNameAction()
    {
        $title = $_POST["knowledgeTitle"];
        $categoryId = $_POST["categoryId"];
        $em = $this->getDoctrine()->getManager();
        $knowledgeObjects = $em->getRepository("OrganizerBundle:Knowledge")->createQueryBuilder("know")
            ->where("know.title LIKE :title")
            ->andWhere("know.categoryId = :categoryId")
            ->setParameter("title", "$title%")
            ->setParameter("categoryId", $categoryId)
            ->getQuery()
            ->getResult();
        $knowledges = [];
        foreach ($knowledgeObjects as $knowledgeObject) {
            $knowledges[] = [
                "id"          => $knowledgeObject->getId(),
                "title"       => $knowledgeObject->getTitle(),
                "content"     => $knowledgeObject->getContent(),
                "categoryId"  => $knowledgeObject->getCategoryId()
            ];
        }
        $response = new Response();
        $response->setContent(json_encode($knowledges));
        return $response;
    }

    public function getTodayKnowledgesAction()
    {
        $repo = $this   ->getDoctrine()
            ->getManager()
            ->getRepository(Knowledge::class);
        $dateParam = date("Y-m-d");
        $knowledges = $repo->createQueryBuilder('x')
            ->select()
            ->where("x.created LIKE :name")
            ->setParameter("name", "$dateParam%")
            ->getQuery()
            ->getResult();
        $knowledgesArray = [];
        $response = new Response();
        foreach ($knowledges as $knowledge) {
            $knowledgesArray[] = [
                "id"          => $knowledge->getId(),
                "title"       => $knowledge->getTitle(),
                "content"     => $knowledge->getContent(),
                "categoryId"  => $knowledge->getCategoryId()
            ];
        }
        $response->setContent(json_encode($knowledgesArray));
        return $response;
    }
}