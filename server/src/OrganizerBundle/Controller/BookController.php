<?php

namespace OrganizerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class BookController extends Controller
{
    const ENGLISH_BOOKS_IDS = [
        3
    ];

    public function getAllAction()
    {
        $em = $this->getDoctrine()->getManager();
        $bookObjects = $em->getRepository('OrganizerBundle:Book')
            ->findAll();
        $response = new Response();
        $books    = [];
        foreach ($bookObjects as $bookObject) {
            $books[] = [
                "id"    => $bookObject->getId(),
                "title" => $bookObject->getTitle(),
                "path"  => $bookObject->getPath()
            ];
        }
        $response->setContent(json_encode($books));
        return $response;
    }

    public function getByIdAction($bookId)
    {
        $em   = $this->getDoctrine()->getManager();
        $book = $em->getRepository('OrganizerBundle:Book')
            ->find($bookId);
        $response = new Response();
        $response->setContent(json_encode(
            [
                "id"    => $book->getId(),
                "title" => $book->getTitle(),
                "path"  => $book->getPath()
            ]
        ));
        return $response;
    }

    public function getEnglishBooksAction()
    {
        $em   = $this->getDoctrine()->getManager();
        $bookObjects = [];
        foreach (self::ENGLISH_BOOKS_IDS as $bookId) {
            $bookObjects[] = $em->getRepository('OrganizerBundle:Book')
                ->find($bookId);
        }
        $response = new Response();
        $books    = [];
        foreach ($bookObjects as $bookObject) {
            $books[] = [
                "id"    => $bookObject->getId(),
                "title" => $bookObject->getTitle(),
                "path"  => $bookObject->getPath()
            ];
        }
        $response->setContent(json_encode($books));
        return $response;
    }
}