<?php

namespace OrganizerBundle\Controller;

use Doctrine\ORM\Tools\Pagination\Paginator;
use OrganizerBundle\Entity\Film;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class FilmController extends Controller
{
    public function getAllAction()
    {
        $limit = 10;
        $page = !empty($_POST["page"])? $_POST["page"] : 1;
        $em = $this->getDoctrine()->getManager();

        $query = $em->getRepository(Film::class)->createQueryBuilder('x')
            ->select()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit)
            ->getQuery();

        $paginator = new Paginator($query, $fetchJoinCollection = false);


        $response = new Response();
        $films = [];
        foreach ($paginator as $film) {
            $films[] = [
                "id"    => $film->getId(),
                "name"  => $film->getName(),
                "url"   => $film->getUrl(),
                "year"  => $film->getYear()
            ];
        }
        $response->setContent(json_encode($films));
        return $response;
    }

    public function getFilmsByNameAction() {
        $name = !empty($_POST["name"]) ? $_POST["name"] : "";
        $limit = 10;
        $page = !empty($_POST["page"])? $_POST["page"] : 1;
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository(Film::class)->createQueryBuilder("film")
            ->where("film.name LIKE :name")
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit)
            ->setParameter("name", "$name%")
            ->getQuery();
        $paginator = new Paginator($query, $fetchJoinCollection = false);


        $response = new Response();
        $films = [];
        foreach ($paginator as $film) {
            $films[] = [
                "id"    => $film->getId(),
                "name"  => $film->getName(),
                "url"   => $film->getUrl(),
                "year"  => $film->getYear()
            ];
        }
        $response->setContent(json_encode($films));
        return $response;
    }

    public function getByIdAction($filmId)
    {
        $em = $this->getDoctrine()->getManager();
        $film = $em->getRepository('OrganizerBundle:Film')
            ->find($filmId);
        $response = new Response();
        $response->setContent(json_encode(
            [
                "id"   => $film->getId(),
                "name" => $film->getName(),
                "url"  => $film->getUrl(),
                "year" => $film->getYear()
            ]
        ));
        return $response;
    }

    public function addFilmAction()
    {
        $em = $this->getDoctrine()->getManager();
        $film = new Film();
        $film->setName(isset($_POST["name"])? $_POST["name"] : "none");
        $film->setUrl(isset($_POST["url"])? $_POST["url"] : "none");
        $em->persist($film);
        $em->flush();
        return new Response();
    }

    public function getTotalCountAction() {
        $repo = $this   ->getDoctrine()
            ->getManager()
            ->getRepository(Film::class);

        $qb = $repo->createQueryBuilder('x');
        $qb->select('COUNT(x)');
        $totalCount = $qb->getQuery()->getSingleScalarResult();
        $response = new Response();
        $response->setContent(json_encode([compact("totalCount")]));
        return $response;
    }

    public function getTotalCountByNameAction() {
        $name = !empty($_POST["name"]) ? $_POST["name"] : "";
        $repo = $this   ->getDoctrine()
            ->getManager()
            ->getRepository(Film::class);

        $qb = $repo->createQueryBuilder('x')
            ->select('COUNT(x)')
            ->where("x.name LIKE :name")
            ->setParameter("name", "$name%");
        $totalCount = $qb->getQuery()->getSingleScalarResult();
        $response = new Response();
        $response->setContent(json_encode([compact("totalCount")]));
        return $response;
    }
}