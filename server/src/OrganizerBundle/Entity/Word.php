<?php

namespace OrganizerBundle\Entity;

/**
 * Word
 */
class Word
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $english;

    /**
     * @var string
     */
    private $russian;

    /**
     * @var string
     */
    private $created;

    /**
     * @return string
     */
    public function getCreatedTime(): string
    {
        return $this->created;
    }

    /**
     * @param string $created
     */
    public function setCreatedTime(string $created)
    {
        $this->created = $created;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set english
     *
     * @param string $english
     *
     * @return Word
     */
    public function setEnglish($english)
    {
        $this->english = $english;

        return $this;
    }

    /**
     * Get english
     *
     * @return string
     */
    public function getEnglish()
    {
        return $this->english;
    }

    /**
     * Set russian
     *
     * @param string $russian
     *
     * @return Word
     */
    public function setRussian($russian)
    {
        $this->russian = $russian;

        return $this;
    }

    /**
     * Get russian
     *
     * @return string
     */
    public function getRussian()
    {
        return $this->russian;
    }
}

