<?php

namespace OrganizerBundle\Entity;

/**
 * Film
 */
class Film
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $kinopoisk_id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $year;

    /**
     * @var string
     */
    private $url;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Film
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Film
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get kinopoisk_id
     *
     * @return int
     */
    public function getKinopoiskId(): int
    {
        return $this->kinopoisk_id;
    }

    /**
     * Set kinopoisk_id
     *
     * @param int $kinopoisk_id
     */
    public function setKinopoiskId(int $kinopoisk_id)
    {
        $this->kinopoisk_id = $kinopoisk_id;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear(): string
    {
        return $this->year;
    }

    /**
     * Set year
     *
     * @param string $year
     */
    public function setYear(string $year)
    {
        $this->year = $year;
    }
}

