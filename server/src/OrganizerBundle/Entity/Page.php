<?php

namespace OrganizerBundle\Entity;

/**
 * Page
 */
class Page
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $bookId;

    /**
     * @var int
     */
    private $numberOfPage;

    /**
     * @var string
     */
    private $content;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bookId
     *
     * @param integer $bookId
     *
     * @return Page
     */
    public function setBookId($bookId)
    {
        $this->bookId = $bookId;

        return $this;
    }

    /**
     * Get bookId
     *
     * @return int
     */
    public function getBookId()
    {
        return $this->bookId;
    }

    /**
     * Set numberOfPage
     *
     * @param integer $numberOfPage
     *
     * @return Page
     */
    public function setNumberOfPage($numberOfPage)
    {
        $this->numberOfPage = $numberOfPage;

        return $this;
    }

    /**
     * Get numberOfPage
     *
     * @return int
     */
    public function getNumberOfPage()
    {
        return $this->numberOfPage;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}

