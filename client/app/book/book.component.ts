import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { BookService } from "./book.service";
import { Book } from "./book";

@Component({
    selector: "book",
    templateUrl: "app/book/book.component.html",
    styleUrls: ["app/book/book.component.css"]
})
export class BookComponent implements OnInit {
    books: Book[];

    constructor (private bookService: BookService, private router:Router) {}

    ngOnInit() {
        this.getAllBooks();
    }

    public getAllBooks() {
        this.bookService.getBooks().subscribe(
            books => this.books = books
        );
    }

    goToBook(id:number) {
        this.router.navigate(["book", id]);
    }
}