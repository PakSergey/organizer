export class Book {
    public id: number;
    public title: string;
    public path: string;

    constructor(id:number, title:string, path:string) {
        this.id = id;
        this.title = title;
        this.path = path;
    }
}