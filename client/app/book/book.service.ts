import { Injectable } from "@angular/core";
import { Headers, Http, Response } from "@angular/http";
import { Book } from "./book";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {TranslatedPage} from "../english/translated-page";

@Injectable()
export class BookService {

    constructor(private http: Http) {}

    public getBooks(): Observable<Book[]> {
        let books = this.http.get("api/getAllBooks")
            .map(this.extractBooks)
            .catch(this.handleError);
        return books;
    }

    public getBook(id: number): Observable<Book> {
        let book = this.http.get("/api/getBook/" + id)
            .map(this.extractBook)
            .catch(this.handleError);
        return book;
    }

    public getEnglishBooks(): Observable<Book[]> {
        let books = this.http.get("api/get-english-books")
            .map(this.extractBooks)
            .catch(this.handleError);
        return books;
    }

    public getPage(bookId:number, numberOfPage:number): Observable<TranslatedPage> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("bookId", bookId.toString());
        params.set("numberOfPage", numberOfPage.toString());
        let words = this.http.post("/api/get-page", params.toString(), { headers: headers })
            .map(this.extractPage)
            .catch(this.handleError);
        return words;
    }

    public savePage(translatedPage: TranslatedPage): Observable<Response> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("bookId", translatedPage.bookId.toString());
        params.set("content", translatedPage.content.toString());
        params.set("numberOfPage", translatedPage.numberOfPage.toString());
        return this.http.post("/api/save-page", params.toString(), { headers: headers })
            .map(function (response: Response) {
                return response;
            })
            .catch(this.handleError);
    }

    private extractBooks(response: Response) {
        let res = response.json();
        let books: Book[] = [];
        for (let i = 0; i < res.length; i++) {
            books.push(new Book(res[i].id, res[i].title, res[i].path));
        }
        return books;
    }

    private extractBook(response: Response) {
        let res = response.json();
        let book = new Book(res.id, res.title, res.path);
        return book;
    }

    private extractPage(response:Response) {
        let res = response.json();
        let page = new TranslatedPage(res.bookId, res.content, res.numberOfPage);
        return page;
    }


    private handleError(error: any, cought: Observable<any>): any {
        let message = "";

        if (error instanceof Response) {
            let errorData = error.json().error || JSON.stringify(error.json());
            message = `${error.status} - ${error.statusText || ''} ${errorData}`
        } else {
            message = error.message ? error.message : error.toString();
        }

        console.error(message);

        return Observable.throw(message);
    }
}

