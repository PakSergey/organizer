import { Component, OnInit } from "@angular/core";
import { ToDoListComponent } from "./todo/to-do-list.component";

@Component({
    selector: "my-app",
    templateUrl: "app/app.component.html",
    styleUrls: ["app/app.component.css"]
}) export class AppComponent implements OnInit{


    toDoHide:boolean = false;

    ngOnInit() {}

    hideTodo() {
        this.toDoHide = true;
    }

    showToDo() {
        this.toDoHide = false;
    }
}