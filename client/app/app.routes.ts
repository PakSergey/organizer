import { Routes } from "@angular/router";
import { FilmComponent } from "./film/film.component";
import { FilmDetailsComponent } from "./film/film-details.component";
import { AddFilmFormComponent } from "./film/add-film-form.component"
import { BookComponent } from "./book/book.component";
import { KnowledgeCategoryComponent } from "./knowledge/knowledge-category.component"
import { KnowledgeComponent } from "./knowledge/knowledge.component"
import { KnowledgeDetailsComponent } from "./knowledge/knowledge-details.component"
import { AddKnowledgeFormComponent } from "./knowledge/add-knowledge-form.component"
import { EditKnowledgeFormComponent } from "./knowledge/edit-knowledge-form.component"
import {EnglishComponent} from  "./english/english.component"


export const routes: Routes = [
    {
        path: "",
        redirectTo: "knowledge",
        pathMatch: "full"
    },
    {
        path: "film",
        component:FilmComponent
    },
    {
        path: "book",
        component:BookComponent
    },
    {
        path: "knowledge",
        component:KnowledgeCategoryComponent
    },
    {
        path: "english",
        component: EnglishComponent
    },
    {
        path: "film/:id",
        component:FilmDetailsComponent
    },
    {
        path: "knowledge/:categoryId",
        component:KnowledgeComponent
    },
    {
        path: "knowledge/knowledge-details/:id",
        component:KnowledgeDetailsComponent
    },
    {
        path: "knowledge-add",
        component:AddKnowledgeFormComponent
    },
    {
        path: "knowledge/edit/:id",
        component:EditKnowledgeFormComponent
    },
    {
        path: "film-add",
        component:AddFilmFormComponent
    }
    // {
    //     path: "film/:id",
    //     component: FilmDetailsComponent
    // }
    // {
    //     path: "phrases",
    //     component: PhraseListComponent
    // },
    // {
    //     path: "phrase/:id",
    //     component: PhraseDetailsComponent
    // }

];