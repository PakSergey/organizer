import {Component, Input, OnInit} from '@angular/core';
import { Router } from "@angular/router";
import {EnglishService} from "./english.service";
import { Word } from "./word";
import {Book} from "../book/book";
import {BookService} from "../book/book.service";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {TranslatedPage} from "./translated-page";


@Component({
    selector: "transate",
    templateUrl: "app/english/translate.component.html",
    styleUrls: ["app/english/translate.component.css"]
})
export class TranslateComponent implements OnInit {

    books: Book[];
    selectedBook: Book;
    bookSelectFlag :boolean = false;
    currentPage:number = 1;
    currentPath:SafeResourceUrl;
    translatedPage: TranslatedPage;

    constructor (private router:Router, private englishService:EnglishService, private bookService: BookService,
                 public sanitizer:DomSanitizer
    ) {}

    ngOnInit() {
        this.bookService.getEnglishBooks().subscribe(
            books => this.books = books
        )
    }

    selectBook(book:Book) {
        this.selectedBook = book;
        this.bookSelectFlag = true;
        this.setPath();
    }

    nextPage() {
        this.currentPage++;
        this.setPath();
    }

    previousPage() {
        this.currentPage--;
        this.setPath();
    }

    setPath() {
        this.currentPath = this.sanitizer.bypassSecurityTrustResourceUrl("");
        setTimeout(() => {
            this.currentPath = this.sanitizer.bypassSecurityTrustResourceUrl(this.selectedBook.path  + "#page=" + this.currentPage);
        }, 0.000001);
    }


    onKeyPress(event:any) {
        if(event.key === "Enter") {
            this.setPath();
        }
    }
}