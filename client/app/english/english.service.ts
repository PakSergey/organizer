import { Injectable } from "@angular/core";
import { Headers, Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import {Word} from "./word";
import {AddWord} from "./add-word";

@Injectable()
export class EnglishService {


    constructor(private http: Http) { }



    public getWords(page:number): Observable<Word[]> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("page", page.toString());
        let words = this.http.post("/api/get-words", params.toString(), { headers: headers })
            .map(this.extractWords)
            .catch(this.handleError);
        return words;
    }

    public getWordsByName(page:number, name:string): Observable<Word[]> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("name", name);
        params.set("page", page.toString());
        return this.http.post("/api/get-words-by-name", params.toString(), { headers: headers })
            .map(this.extractWords)
            .catch(this.handleError);
    }

    public getTotalCount(): Observable<number> {
        return this.http.get("/api/get-words-total-count")
            .map(function (response:Response) {
                let res = response.json();
                return res[0].totalCount;
            })
            .catch(this.handleError);
    }

    public getTotalCountByName(name:string): Observable<number> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("name", name);
        return this.http.post("/api/get-words-total-count-by-name", params.toString(), { headers: headers })
            .map(function (response:Response) {
                let res = response.json();
                return res[0].totalCount;
            })
            .catch(this.handleError);
    }

    public addWord(word:AddWord): Observable<Response> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("english", word.english);
        params.set("russian", word.russian);
        return this.http.post("/api/add-word", params.toString(), {headers: headers})
            .map(function (response:Response) {
                return response;
            })
            .catch(this.handleError);
    }

    public getWordsByOptions(typeOfWords:string, count:number): Observable<Word[]> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("count", count.toString());
        params.set("typeOfWords", typeOfWords);
        return this.http.post("/api/get-words-by-options", params.toString(), {headers: headers})
            .map(this.extractWords)
            .catch(this.handleError);
    }



    private extractWords(response: Response) {
        let res = response.json();
        let films: Word[] = [];
        for (let i = 0; i < res.length; i++) {
            films.push(new Word(res[i].id, res[i].english, res[i].russian));
        }
        return films;
    }

    private handleError(error: any, cought: Observable<any>): any {
        let message = "";
        if (error instanceof Response) {
            let errorData = error.json().error || JSON.stringify(error.json());
            message = `${error.status} - ${error.statusText || ''} ${errorData}`
        } else {
            message = error.message ? error.message : error.toString();
        }

        console.error(message);

        return Observable.throw(message);
    }
}

