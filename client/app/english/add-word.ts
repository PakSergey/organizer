export class AddWord {
    public english: string;
    public russian: string;

    constructor(english:string, russian:string) {
        this.english = english;
        this.russian = russian;
    }
}