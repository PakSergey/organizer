import {Component, Input, OnInit} from '@angular/core';
import { Router } from "@angular/router";
import {EnglishService} from "./english.service";
import { Word } from "./word";


@Component({
    selector: "test",
    templateUrl: "app/english/test.component.html",
    styleUrls: ["app/english/test.component.css"]
})
export class TestComponent implements OnInit {

    typeOfWords:string = "All";
    count:number = 20;
    words: Word[];
    currentQuestionNumber:number = 0;
    currentQuestion: Word;
    correctAnswersCount:number = 0;
    answer:string;
    test_started:boolean = false;
    test_finished:boolean = false;
    wrongAnswers: Word[] = [];

    constructor (private router:Router, private englishService:EnglishService) {}

    ngOnInit() {}

    startTest() {
        this.englishService.getWordsByOptions(this.typeOfWords, this.count).subscribe(
            words => {
                this.words = words;
                this.refreshTest();
            }

        );
        this.test_started = true;
    }

    refreshTest() {
        this.test_finished = false;
        this.currentQuestionNumber = 0;
        this.correctAnswersCount = 0;
        this.wrongAnswers = [];
        this.currentQuestion = this.words[this.currentQuestionNumber];
    }

    nextQuestion() {
        if(this.answer == this.words[this.currentQuestionNumber].engilsh) {
            this.correctAnswersCount++;
        }
        else {
            this.wrongAnswers.push(this.words[this.currentQuestionNumber]);
        }
        this.answer = null;
        this.currentQuestionNumber++;
        if(this.currentQuestionNumber == this.words.length) {
            this.test_finished = true;
        }
        else {
            this.currentQuestion = this.words[this.currentQuestionNumber];
        }
    }
}