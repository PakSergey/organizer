import {Component, Input, OnInit} from '@angular/core';
import { Router } from "@angular/router";
import {EnglishService} from "./english.service";
import { Word } from "./word";


@Component({
    selector: "dictionary",
    templateUrl: "app/english/dictionary.component.html",
    styleUrls: ["app/english/dictionary.component.css"]
})
export class DictionaryComponent implements OnInit {


    searchInput: string;
    words:Word[];
    totalCount: number;
    searchFlag:boolean = false;



    constructor (private router:Router, private englishService:EnglishService) {}

    ngOnInit() {
        this.englishService.getTotalCount().subscribe(
            totalCount => this.totalCount = totalCount
        );
        this.getWords();
    }

    searchWord(name:any) {
        this.searchFlag = true;
        this.englishService.getTotalCountByName(name).subscribe(
            totalCount => this.totalCount = totalCount
        );
        this.getWords();
    }

    getWords(page:number = 1) {
        if(this.searchFlag) {
            this.englishService.getWordsByName(page, this.searchInput).subscribe(
                words => {
                    this.words      = words;
                    this.searchFlag = false;
                }
            );
        } else {
            this.englishService.getWords(page).subscribe(
                words => {this.words = words;
                }
        );
        }
    }

    paginate(event:any) {
        this.getWords(event.page + 1);
    }

}