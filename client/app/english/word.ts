export class Word {
    public id: number;
    public engilsh: string;
    public russian: string;

    constructor(id:number, english:string, russian:string) {
        this.id = id;
        this.engilsh = english;
        this.russian = russian;
    }
}
