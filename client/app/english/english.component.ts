import {Component, OnInit, ViewChild} from '@angular/core';
import { Router } from "@angular/router";
import { AddWord } from "./add-word";
import { EnglishService } from "./english.service";
import {DictionaryComponent} from "./dictionary.component";


@Component({
    selector: "english",
    templateUrl: "app/english/english.component.html",
    styleUrls: ["app/english/english.component.css"]
})
export class EnglishComponent implements OnInit {

    @ViewChild(DictionaryComponent) dictionaryComponent: DictionaryComponent;

    page:string = "dictionary";
    word:AddWord = new AddWord(null, null);

    constructor (private router:Router, private englishService:EnglishService) {}

    ngOnInit() {}

    navigateTo(page:string) {
        this.page = page;
    }

    addWord() {
        if(this.word.russian != null || this.word.english != null) {
            this.englishService.addWord(this.word).subscribe(
                response => {
                    if(response.status == 200) {
                        this.word.russian = null;
                        this.word.english = null;
                        this.dictionaryComponent.getWords();
                    }
                }
            )
        }
    }

    onKeyPress(event:any) {
        if(event.key === "Enter") {
            this.addWord();
        }
    }
}