export class TranslatedPage {
    public bookId: number;
    public content: string ;
    public numberOfPage: number;

    constructor(bookId:number, content:string, numberOfPage:number) {
        this.bookId = bookId;
        this.content = content;
        this.numberOfPage = numberOfPage;
    }
}
