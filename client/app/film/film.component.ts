import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FilmService } from "./film.service";
import { Film } from "./film";

@Component({
    selector: "film",
    templateUrl: "app/film/film.component.html",
    styleUrls: ["app/film/film.component.css"]
})
export class FilmComponent implements OnInit {

    films: Film[];
    searchInput: string;
    totalCount: number;
    searchFlag: boolean = false;

    constructor (private filmService: FilmService, private router:Router) {}

    ngOnInit() {
        this.getAllFilms();
        this.filmService.getTotalCount().subscribe(
            total => this.totalCount = total
        );
    }
    public getAllFilms(page = 1) {
        this.filmService.getFilms(page).subscribe(
            films => this.films = films
        );
    }

    goToFilm(id:number) {
        this.router.navigate(["film", id]);
    }

    searchFilm(query:any) {
        this.searchFlag = true;
        this.filmService.getTotalCountByName(this.searchInput).subscribe(
            total => this.totalCount = total
        );
        this.getFilms();
    }

    goToAddingFilmForm() {
        this.router.navigate(["film-add"]);
    }

    paginate(event:any) {
        this.getFilms(event.page + 1);
    }

    getFilms(page = 1) {
        if(this.searchFlag) {
            this.filmService.getFilmsByName(page, this.searchInput).subscribe(
                films => this.films = films
            );
        } else {
            this.getAllFilms(page);
        }
    }
}