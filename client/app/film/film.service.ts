import { Injectable } from "@angular/core";
import { Headers, Http, Response } from "@angular/http";
import { Film } from "./film";
import { AddFilmForm } from "./add-film-form"

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FilmService {

    constructor(private http: Http) { }

    // Отправка GET запроса нв сервер
    public getFilms(page:number): Observable<Film[]> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("page", page.toString());
        let films = this.http.post("/api/getAllFilms", params.toString(), { headers: headers })
            .map(this.extractFilms)
            .catch(this.handleError);
        return films;
    }

    public getFilmsByName(page:number, name:string): Observable<Film[]> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("page", page.toString());
        params.set("name", name);
        return this.http.post("/api/get-films-by-name", params.toString(), { headers: headers })
            .map(this.extractFilms)
            .catch(this.handleError);
    }

    public getFilm(id: number): Observable<Film> {
        let film = this.http.get("/api/getFilm/" + id)
            .map(this.extractFilm)
            .catch(this.handleError);
        return film;
    }

    public addFilm(filmForm:AddFilmForm): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("url", filmForm.url);
        params.set("name", filmForm.name);
        return this.http.post("/api/add-film", params.toString(), { headers: headers })
            .map(function (response: Response) {
                return response;
            })
            .catch(this.handleError);
    }

    public getTotalCount(): Observable<any> {
        let total = this.http.get("/api/get-films-total-count")
            .map(function (response:Response) {
                let res = response.json();
                return res[0].totalCount;
            })
            .catch(this.handleError);
        return total;
    }

    public getTotalCountByName(name:string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("name", name);
        return this.http.post("/api/get-films-total-count-by-name", params.toString(), { headers: headers })
            .map(function (response:Response) {
                let res = response.json();
                return res[0].totalCount;
            })
            .catch(this.handleError);
    }




    private extractFilms(response: Response) {
        let res = response.json();
        let films: Film[] = [];
        for (let i = 0; i < res.length; i++) {
            films.push(new Film(res[i].id, res[i].name, res[i].year, res[i].url));
        }
        return films;
    }

    private extractFilm(response: Response) {
        let res = response.json();
        let film = new Film(res.id, res.name, res.year, res.url);
        return film;
    }

    private handleError(error: any, cought: Observable<any>): any {
        let message = "";

        if (error instanceof Response) {
            let errorData = error.json().error || JSON.stringify(error.json());
            message = `${error.status} - ${error.statusText || ''} ${errorData}`
        } else {
            message = error.message ? error.message : error.toString();
        }

        console.error(message);

        return Observable.throw(message);
    }
}

