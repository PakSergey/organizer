export class Film {
    public id: number;
    public name: string;
    public url: string;
    public year: string;

    constructor(id:number, name:string, year:string, url:string ) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.year = year;
    }
}