import {Component} from '@angular/core';
import {FilmService} from './film.service';
import {AddFilmForm} from './add-film-form';
import {Film} from "./film";
import {ActivatedRoute, Router} from "@angular/router";
import {Message} from "primeng/primeng";

@Component({
    selector: "add-film",
    templateUrl: "app/film/add-film-form.component.html",
    styleUrls: ["app/film/add-film-form.component.css"]
})
export class AddFilmFormComponent {
    form: AddFilmForm;
    name:string;
    url:string;
    msgs:Message[] = [];

    constructor(private filmService: FilmService, private router:Router, private route: ActivatedRoute) {}

    onAddFilm() {
        let film:AddFilmForm = new AddFilmForm(this.name, this.url);
        this.filmService.addFilm(film).subscribe(
            response => {
                if(response.status == 200) {
                    this.msgs.push({severity:'success', summary:'Фильм добавлен!!!', detail:'Красавчик'});
                }
            }
        );
        setTimeout(() => {
            this.router.navigate(["film"]);
        }, 1000);
    }

    checkBtn():boolean {
        if(!this.name || !this.url) {
            return true;
        }
        return false;
    }
}