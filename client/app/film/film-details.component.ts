import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from "@angular/router";
import { FilmService } from "./film.service";
import { Film } from "./film";
import { SafePipe } from "../safe.pipe";
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';


@Component({
    selector: "film-details",
    templateUrl: "app/film/film-details.component.html",
    styleUrls: ["app/film/film-details.component.css"],
})
export class FilmDetailsComponent implements OnInit {

    film: Film;

    constructor (public filmService: FilmService,
                 public activatedRoute:ActivatedRoute,
                 public sanitizer:DomSanitizer
                 ) {}

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            let id = +params["id"]; // конвертируем значение параметра id в тип number
            this.getFilm(id);
        });
        // let a = this.filmCode;
    }

    getFilm(id:number) {
        this.filmService.getFilm(id).subscribe(
            film => {
                this.film = film;
            }
        );
    }
}