"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
// import { RouterModule } from "@angular/router";
var app_component_1 = require("./app.component");
// import { routes } from "./app.routes";
// // components
// import { HomeComponent } from "./home/home.component";
// import { PhraseDetailsComponent } from "./phrase-details/phrase-details.component";
// import { PhraseListComponent } from "./phrase-list/phrase-list.component";
// // services
// import { PhraseService } from "./shared/phrase.service";
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule
            // RouterModule.forRoot(routes)
        ],
        declarations: [app_component_1.AppComponent],
        // providers: [PhraseService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map