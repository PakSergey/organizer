import { Injectable } from "@angular/core";
import {Headers, Http, Response} from "@angular/http";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ToDo} from "./to-do";

@Injectable()
export class TodoService {

    constructor(private http: Http) { }

    public addTodo(text:string) : Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("text", text);
        return this.http.post("/api/add-todo", params.toString(), { headers: headers })
            .map(function (response: Response) {
                return response;
            })
            .catch(this.handleError);
    }

    public getTodoList() : Observable<any>{
        let todoList = this.http.get("/api/get-todo-list")
            .map(this.extractTodoList)
            .catch(this.handleError);
        return todoList;
    }

    public deleteTodo(todo:ToDo) : Observable<any>{
        let knowledge = this.http.get("/api/delete-todo-by-id/" + +todo.id)
            .map(function (response: Response) {
                return response;
            })
            .catch(this.handleError);
        return knowledge;
    }



    //extract методы
    private extractTodoList(response: Response) {
        let res = response.json();
        let todoList: ToDo[] = [];
        for (let i = 0; i < res.length; i++) {
            todoList.push(new ToDo(res[i].id, res[i].status, res[i].value));
        }
        return todoList;
    }

    //Обработчик ошибок
    private handleError(error: any, cought: Observable<any>): any {
        let message = "";

        if (error instanceof Response) {
            let errorData = error.json().error || JSON.stringify(error.json());
            message = `${error.status} - ${error.statusText || ''} ${errorData}`
        } else {
            message = error.message ? error.message : error.toString();
        }

        console.error(message);

        return Observable.throw(message);
    }
}

