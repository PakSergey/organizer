import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ToDo} from './to-do';
import {TodoService} from './to-do.service';

@Component({
    selector: "to-do-line",
    templateUrl: "app/todo/to-do-line.component.html",
    styleUrls: ["app/todo/to-do-line.component.css"]
})
export class ToDoLineComponent {

    @Input() toDo: ToDo;

    @Output() deleteTodo = new EventEmitter();


    constructor(private todoService:TodoService) {}


    onDelete() {
        this.deleteTodo.emit(this.toDo);
    }


}