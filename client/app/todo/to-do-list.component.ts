import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { Router } from "@angular/router";
import {ToDo} from './to-do';
import {TodoService} from './to-do.service';

@Component({
    selector: "to-do",
    templateUrl: "app/todo/to-do-list.component.html",
    styleUrls: ["app/todo/to-do-list.component.css"]
})
export class ToDoListComponent implements OnInit{

    todoList:ToDo[];
    inputText: string;

    @Output() hideTodo = new EventEmitter();

    constructor(private todoService:TodoService) {}

    ngOnInit() {
        this.refreshTodoList();
    }


    onDelete(toDo:ToDo) {
        this.todoService.deleteTodo(toDo).subscribe(
            response => {
                if(response.status == 200) {
                    this.refreshTodoList();
                }
            }
        )
    }

    addTodo() {
        this.todoService.addTodo(this.inputText).subscribe(
            response => {
                if(response.status == 200) {
                    this.refreshTodoList();
                }
                this.inputText = null;
            }
        );
    }

    refreshTodoList() {
        this.todoService.getTodoList().subscribe(
            todoList => {
                this.todoList = todoList;
            }
        )
    }

    onKeyPress(event:any) {
        if (event.key === "Enter" && this.inputText != null) {
            this.addTodo();
        }
    }

    hide() {
        this.hideTodo.emit();
    }
}