export class ToDo {
    public id: number;
    public status: string;
    public value: string;

    constructor(id:number, status:string, value:string) {
        this.id = id;
        this.status = status;
        this.value = value;
    }
}