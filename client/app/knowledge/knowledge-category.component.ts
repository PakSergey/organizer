import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { KnowledgeService } from "./knowledge.service";
import { KnowledgeCategory } from "./knowledgeCategory";
import {Knowledge} from "./knowledge";

@Component({
    selector: "knowledge-category",
    templateUrl: "app/knowledge/knowledge-category.component.html",
    styleUrls: ["app/knowledge/knowledge-category.component.css"]
})
export class KnowledgeCategoryComponent implements OnInit {

    knowledgeCategories: KnowledgeCategory[];
    searchInput: string;
    todayKnowledges: Knowledge[];

    constructor (private knowledgeService: KnowledgeService, private router:Router) {}

    ngOnInit() {
        this.getAllCategories();
        this.knowledgeService.getTodayKnowledges().subscribe(
            knowledges => this.todayKnowledges = knowledges
        )
    }

    getAllCategories() {
        this.knowledgeService.getKnowledgeCategories().subscribe(
            categories => this.knowledgeCategories = categories
        );
    }

    goToKnowledgesByCategory(categoryId:number) {
        this.router.navigate(["knowledge", categoryId]);
    }

    goToAddingKnowledgeForm() {
        this.router.navigate(["knowledge-add"]);
    }

    searchCategory(query:any) {
        this.knowledgeService.getCategoriesByName(query).subscribe(
            categoryList => {
                this.knowledgeCategories = categoryList;
            }
        );
    }

    goToKnowledgeById(id:number) {
        this.router.navigate(["knowledge/knowledge-details", id])
    }
}