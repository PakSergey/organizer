export class Knowledge {
    public id?: number;
    public categoryId: number;
    public title: string;
    public content: string;

    constructor(categoryId?:number, title?:string, content?:string, id?:number) {
        this.id = id;
        this.title = title;
        this.categoryId = categoryId;
        this.content = content;
    }
}