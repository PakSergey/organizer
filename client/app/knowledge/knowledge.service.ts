import { Injectable } from "@angular/core";
import {Headers, Http, Response} from "@angular/http";
import { Knowledge } from "./knowledge";
import { KnowledgeCategory } from "./knowledgeCategory";


import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class KnowledgeService {

    constructor(private http: Http) { }

    //Категории
    public getKnowledgeCategories() : Observable<KnowledgeCategory[]> {
        let categories = this.http.get("/api/getAllCategories")
            .map(this.extractCategories)
            .catch(this.handleError);
        return categories;
    }

    public getCategoriesByName(name:any) : Observable<KnowledgeCategory[]>{
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("categoryName", name);
        let categoryList = this.http.post("/api/get-categories-by-name", params.toString(), {headers : headers})
            .map(this.extractCategories)
            .catch(this.handleError);
        return categoryList;
    }

    public getCategoryById(id:number) : Observable<KnowledgeCategory> {
        let category = this.http.get("/api/get-category-by-id/" + +id)
            .map(this.extractCategory)
            .catch(this.handleError);
        return category;
    }

    //Знания
    public getKnowledgesByCategoryId(categoryId:number) : Observable<Knowledge[]> {
        let knowledges = this.http.get("/api/getKnowledgesByCategoryId/" + +categoryId)
            .map(this.extractKnowledges)
            .catch(this.handleError);
        return knowledges;
    }

    public getKnowledgeById(id:number) : Observable<Knowledge> {
        let knowledge = this.http.get("/api/getKnowledgeById/" + +id)
            .map(this.extractKnowledge)
            .catch(this.handleError);
        return knowledge;
    }

    public getKnowledgesByName(title:any, categoryId:any) : Observable<Knowledge[]>{
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("knowledgeTitle", title);
        params.set("categoryId", categoryId);
        let knowledgesList = this.http.post("/api/get-knowledges-by-name", params.toString(), {headers : headers})
            .map(this.extractKnowledges)
            .catch(this.handleError);
        return knowledgesList;
    }

    public getTodayKnowledges() : Observable<Knowledge[]>{
        let knowledges = this.http.get("/api/get-today-knowledges")
            .map(this.extractKnowledges)
            .catch(this.handleError);
        return knowledges;
    }

    public addKnowledge(knowledge:any) : Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("categoryName", knowledge.categoryName);
        params.set("title", knowledge.title);
        params.set("content", knowledge.content);
        return this.http.post("/api/addKnowledge", params.toString(), { headers: headers })
            .map(function (response: Response) {
                return response;
            })
            .catch(this.handleError);
    }

    public updateKnowledge(knowledge:any) : Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let params = new URLSearchParams();
        params.set("id", knowledge.id);
        params.set("content", knowledge.content);
        return this.http.post("/api/update-knowledge", params.toString(), { headers: headers })
            .map(function (response: Response) {
                return response;
            })
            .catch(this.handleError);
    }

    public deleteKnowledgeById(id:number) : Observable<any> {
        let knowledge = this.http.get("/api/delete-knowledge-by-id/" + +id)
            .map(function (response: Response) {
                return response;
            })
            .catch(this.handleError);
        return knowledge;
    }
    


    //extract методы
    private extractCategories(response: Response) {
        let res = response.json();
        let categories: KnowledgeCategory[] = [];
        for (let i = 0; i < res.length; i++) {
            categories.push(new KnowledgeCategory(res[i].id, res[i].name));
        }
        return categories;
    }

    private extractCategory(response: Response) {
        let res = response.json();
        let category: KnowledgeCategory = new KnowledgeCategory(res.id, res.name);
        return category;
    }

    private extractKnowledges(response: Response) {
        let res = response.json();
        let knowledges: Knowledge[] = [];
        for (let i = 0; i < res.length; i++) {
            knowledges.push(new Knowledge(res[i].categoryId, res[i].title, res[i].content, res[i].id));
        }
        return knowledges;
    }

    private extractKnowledge(response: Response) {
        let res = response.json();
        let knowledge: Knowledge = new Knowledge(res.categoryId, res.title, res.content, res.id);
        return knowledge;
    }

    //Обработчик ошибок
    private handleError(error: any, cought: Observable<any>): any {
        let message = "";

        if (error instanceof Response) {
            let errorData = error.json().error || JSON.stringify(error.json());
            message = `${error.status} - ${error.statusText || ''} ${errorData}`
        } else {
            message = error.message ? error.message : error.toString();
        }

        console.error(message);

        return Observable.throw(message);
    }
}

