import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { KnowledgeService } from "./knowledge.service";
import { Knowledge } from "./knowledge";
import { SafePipe } from "../safe.pipe";
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';
import {Message} from "primeng/primeng";


@Component({
    selector: "knowledge-details",
    templateUrl: "app/knowledge/knowledge-details.component.html",
    styleUrls: ["app/knowledge/knowledge-details.component.css"],
})
export class KnowledgeDetailsComponent implements OnInit {

    knowledge: Knowledge;
    msgs:Message[] = [];

    constructor (public knowledgeService: KnowledgeService,
                 public activatedRoute:ActivatedRoute,
                 public sanitizer:DomSanitizer,
                 private router:Router
                ) {}

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            let id = +params["id"]; // конвертируем значение параметра id в тип number
            this.getKnowledge(id);
        });
    }

    getKnowledge(id:number) {
        this.knowledgeService.getKnowledgeById(id).subscribe(
            knowledge => {
                this.knowledge = knowledge;
            }
        );
    }

    onEditKnowledge() {
        this.router.navigate(["knowledge/edit", this.knowledge.id]);
    }
    onDeleteKnowledge() {
        this.knowledgeService.deleteKnowledgeById(this.knowledge.id).subscribe(
            response => {
                if(response.status == 200) {
                    this.msgs.push({severity:'success', summary:'Знание удалено!!!', detail:'Красавчик'});
                }
            }
        );
        setTimeout(() => {
            this.router.navigate(["knowledge"]);
        }, 1000);
    }
}