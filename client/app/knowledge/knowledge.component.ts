import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { KnowledgeService } from "./knowledge.service";
import { Knowledge } from "./knowledge";
import {DomSanitizer} from '@angular/platform-browser';
import {KnowledgeCategory} from "./knowledgeCategory";


@Component({
    selector: "knowledge",
    templateUrl: "app/knowledge/knowledge.component.html",
    styleUrls: ["app/knowledge/knowledge.component.css"],
})
export class KnowledgeComponent implements OnInit {

    knowledges: Knowledge[];
    category: KnowledgeCategory;
    searchInput:string;

    constructor (public knowledgeService: KnowledgeService,
                 public activatedRoute:ActivatedRoute,
                 private router:Router
                 ) {}

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            let categoryId = +params["categoryId"];
            this.knowledgeService.getCategoryById(categoryId).subscribe(
                category => {
                    this.category = category;
                }
            );
            this.getKnowledgesByCategoryId(categoryId);
        });
    }

    getKnowledgesByCategoryId(categoryId:number) {
        this.knowledgeService.getKnowledgesByCategoryId(categoryId).subscribe(
            knowledges => {
                this.knowledges = knowledges;
            }
        );
    }

    goToKnowledge(id:number) {
        this.router.navigate(["knowledge/knowledge-details", id])
    }

    goToAddingKnowledgeForm() {
        let categoryName:string;
        this.router.navigate(["knowledge-add"], {queryParams: {category: this.category.name}});
    }

    searchKnowledge(query:any) {
        this.knowledgeService.getKnowledgesByName(query, this.category.id).subscribe(
            knowledgeList => {
                this.knowledges = knowledgeList;
            }
        );
    }
}