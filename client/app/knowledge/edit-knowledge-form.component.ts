import {Component, OnInit} from '@angular/core';
import {KnowledgeService} from './knowledge.service';
import { ActivatedRoute, Params, Router } from "@angular/router";
import {Knowledge} from "./knowledge";
import {Message} from "primeng/primeng";

@Component({
    selector: "edit-knowledge",
    templateUrl: "app/knowledge/edit-knowledge-form.component.html",
    styleUrls: ["app/knowledge/edit-knowledge-form.component.css"]
})
export class EditKnowledgeFormComponent implements OnInit {

    knowledge: Knowledge;
    categoryName: string;
    msgs:Message[] = [];

    constructor (public knowledgeService: KnowledgeService,
                 public activatedRoute:ActivatedRoute,
                 private router:Router
    ) {
        this.knowledge = new Knowledge();
    }

    ngOnInit() {
        this.activatedRoute.params.forEach((params: Params) => {
            let id = +params["id"]; // конвертируем значение параметра id в тип number
            this.getKnowledge(id);
        });
    }

    getKnowledge(id:number) {
        this.knowledgeService.getKnowledgeById(id).subscribe(
            knowledge => {
                this.knowledge = knowledge;
                this.knowledgeService.getCategoryById(knowledge.categoryId).subscribe(
                    category => this.categoryName = category.name
                )
            }
        );
    }

    onSaveKnowledge() {
        this.knowledgeService.updateKnowledge(this.knowledge).subscribe(
            response => {
                if(response.status == 200) {
                    this.msgs.push({severity:'success', summary:'Знание отредактировано!!!', detail:'Красавчик'});
                }
            }
        )
        setTimeout(() => {
            this.router.navigate(["knowledge"]);
        }, 1000);
    }
}