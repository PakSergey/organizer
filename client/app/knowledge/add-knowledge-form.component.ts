import {Component, OnInit} from '@angular/core';
import {KnowledgeService} from './knowledge.service';
import {KnowledgeCategory} from './knowledgeCategory';
import {AddKnowledgeForm} from './add-knowledge-form';
import {Knowledge} from "./knowledge";
import {ActivatedRoute, Router} from "@angular/router";
import {Message} from "primeng/primeng";

@Component({
    selector: "add-knowledge",
    templateUrl: "app/knowledge/add-knowledge-form.component.html",
    styleUrls: ["app/knowledge/add-knowledge-form.component.css"]
})
export class AddKnowledgeFormComponent implements OnInit {
    form: AddKnowledgeForm;
    category: string;
    categoryList: KnowledgeCategory[];
    title:string;
    content:string;
    msgs:Message[] = [];

    constructor(private knowledgeService: KnowledgeService, private router:Router, private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.category = params["category"] || null;
        });
    }

    filterCategory(event:any) {
        let query = event.query;
        this.knowledgeService.getCategoriesByName(query).subscribe(
            categoryList => {
                this.categoryList = categoryList;
            }
        );
    }
    onAddKnowledge() {
        let knowledge:AddKnowledgeForm = new AddKnowledgeForm(this.category, this.title, this.content);
        this.knowledgeService.addKnowledge(knowledge).subscribe(
            response => {
                if(response.status == 200) {
                    this.msgs.push({severity:'success', summary:'Знание добавлено!!!', detail:'Красавчик'});
                }
            }
        );
        setTimeout(() => {
            this.router.navigate(["knowledge"]);
        }, 1000);
    }

    checkBtn():boolean {
        if(!this.content || !this.title || !this.category) {
            return true;
        }
        return false;
    }
}