export class AddKnowledgeForm {
    public categoryName: string;
    public title: string;
    public content: string;

    constructor(categoryName:string, title:string, content:string) {
        this.title = title;
        this.categoryName = categoryName;
        this.content = content;
    }
}