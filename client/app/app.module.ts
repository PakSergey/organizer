import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { HttpModule } from "@angular/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteModule, InputTextareaModule, ButtonModule, MessagesModule, InputTextModule, EditorModule, PaginatorModule} from 'primeng/primeng';

// components
import { AppComponent } from "./app.component";
import { routes } from "./app.routes";
import { FilmComponent } from "./film/film.component";
import { BookComponent } from "./book/book.component";
import { FilmDetailsComponent } from "./film/film-details.component";
import { AddFilmFormComponent } from "./film/add-film-form.component"
import { KnowledgeCategoryComponent } from "./knowledge/knowledge-category.component";
import { KnowledgeComponent } from "./knowledge/knowledge.component";
import { KnowledgeDetailsComponent } from "./knowledge/knowledge-details.component";
import { AddKnowledgeFormComponent } from "./knowledge/add-knowledge-form.component"
import { EditKnowledgeFormComponent } from "./knowledge/edit-knowledge-form.component";
import { ToDoListComponent } from "./todo/to-do-list.component";
import { ToDoLineComponent } from "./todo/to-do-line.component";
import { EnglishComponent }  from "./english/english.component";
import { DictionaryComponent } from "./english/dictionary.component";
import { TestComponent } from "./english/test.component";
import { TranslateComponent } from "./english/translate.component";


// services
import { FilmService } from "./film/film.service";
import { KnowledgeService } from "./knowledge/knowledge.service";
import { BookService } from "./book/book.service";
import { TodoService } from "./todo/to-do.service";
import { EnglishService } from "./english/english.service";


// pipes
import { SafePipe } from "./safe.pipe";


@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        AutoCompleteModule,
        InputTextareaModule,
        ButtonModule,
        MessagesModule,
        InputTextModule,
        EditorModule,
        PaginatorModule
    ],
    declarations: [AppComponent, FilmComponent,
        FilmDetailsComponent, AddFilmFormComponent, SafePipe, BookComponent,
        KnowledgeCategoryComponent,
        KnowledgeComponent,KnowledgeDetailsComponent,
        AddKnowledgeFormComponent, EditKnowledgeFormComponent,
        ToDoListComponent, ToDoLineComponent, EnglishComponent, DictionaryComponent,TestComponent, TranslateComponent
    ],
    providers: [FilmService, BookService, KnowledgeService, TodoService, EnglishService],
    bootstrap: [AppComponent]
})
export class AppModule { }
